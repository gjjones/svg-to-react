'use strict';

const assert = require('chai').assert;
const utilities = require('../src/utilities.js');

describe('getFileInfo', function() {
  const getFileInfo = utilities.getFileInfo;
  const optimizeSVG = utilities.optimizeSVG;

  it('should return filename from path', function() {
    const filename = getFileInfo('test\\svgs\\simple.svg').filename;
    assert.equal(
      filename, 'simple.svg',
      'getFileInfo strips pathing from filename');
  });

  it('should return contents from file', function() {
    const fileData = getFileInfo('test\\svgs\\simple.svg').data;
    assert(
      fileData.startsWith('<svg'),
      'getFileInfo reads file contents to data property');
  });

  it('should remove xmlns from attributes', function() {
    const fileData = getFileInfo('test\\svgs\\simple.svg').data;
    const markup = optimizeSVG(fileData);
    assert(
      !markup.includes('xmlns'),
      'optimizeSVG should remove xmlns from svg element attributes');
  });

  it('should add React specific attributes', function() {
    const fileData = getFileInfo('test\\svgs\\simple.svg').data;
    const markup = optimizeSVG(fileData);
    assert(
      markup.includes('className={className}'),
      'optimizeSVG should add the className attribute to the SVG');
    assert(
      markup.includes('{...other}'),
      'optimizeSVG should add the property spread to the SVG');
  });
});

describe('stringifyAsReact', function() {
  const stringifyAsReact = utilities.stringifyAsReact;
  const fileInfo = {
    filename: 'super-simple.svg',
    markup: '<svg xmlns="https://www.w3.org/2000/svg"/>'
  };

  it('should create component name from filename', function() {
    const componentName = stringifyAsReact(fileInfo).componentName;
    assert.equal(
      componentName, 'SuperSimpleIcon',
      'create component name from filename');
  });
});
