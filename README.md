# svg-to-react

This command line tool will convert svg files to a collection of stateless React components that you can import into your project.


## Getting Started

_This tool is not currently in npm._

Start by installing the tool to your project.

```
 $ npm install --save-dev buzz-svg-to-react
```

When including in your project, go ahead and add to your node build tools:

```javascript
//package.json
...
  "scripts": {
    "build": "buzz-svg-to-react -i \"input-glob/*.svg\" -o outputFile.js"
  }
...
```

## Usage

Once this tool has been run, you will have a ready-to-use file of icons to include in your project.

Here is an example of using a generated icon file named "icons-library.js"

```javascript
import { SearchIcon } from './icons-library.js';
const SearchIconButton = (props) => {
  <button class="btn">
    props.text
    <SearchIcon className="btn__icon" />
  </button>
}
```

## Arguments

```
 $ buzz-svg-to-react --help

  Usage: index [options]

  Options:

    -h, --help           output usage information
    -V, --version        output the version number
    -i, --input [glob]   Specify input path to SVGs
    -o, --output [path]  Specify output path for react components
```

### input: -i, --input [glob]
This glob will grab the svgs to import.

### output: -o, --output [path]
The output file is where the react components source file will write to
