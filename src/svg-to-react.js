'use strict';

const glob = require('glob');
const by = require('sort-by');
const utilities = require('./utilities.js');

const getFileInfo = utilities.getFileInfo,
  optimizeSVG = utilities.optimizeSVG,
  stringifyAsReact = utilities.stringifyAsReact;

const fileTmpl = (componentsSource) => (
`import React from 'react';

${componentsSource}`);

module.exports = (svgGlob) => {
  const reactComponents = glob.sync(svgGlob)
    .map(getFileInfo)
    .map(file => Object.assign({}, file, { markup: optimizeSVG(file.data) }))
    .map(stringifyAsReact)
    .sort(by('componentName'));

  const componentsSource = reactComponents.reduce(
    (acc, component) => acc + component.source,
    '');
  return fileTmpl(componentsSource);
}
