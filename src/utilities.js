'use strict';

const fs = require('fs');
const path = require('path');
const SVGO = require('svgo');
const svgo = new SVGO({
  plugins: [
    { 'removeXMLNS': true },
    {
      'addAttributesToSVGElement': {
        attributes: ["className={className}", "{...other}"]
      }
    },
  ]
});
const titleCase = require('title-case');

const componentTmpl = (componentName, markup) => {
return `export const ${componentName} = ({className, ...other}) => (
  ${markup}
);
`};

module.exports = {
  getFileInfo: (filepath) => {
    const filename = filepath.split(path.sep).pop();
    const fileContents = fs.readFileSync(filepath, 'utf8');
    return {
      filename,
      data: fileContents
    };
  },
  optimizeSVG: (svg) => {
    let optimizedSVG;
    svgo.optimize(svg, result => optimizedSVG = result.data);
    return optimizedSVG;
  },
  stringifyAsReact: (svg) => {
    let filename = path.basename(svg.filename, '.svg'),
      nameParts = filename.split('-'),
      componentName = [
        ...nameParts.map(titleCase),
        'Icon'
      ].join(''),
      markup = svg.markup;
    return {
      componentName,
      source: componentTmpl(componentName, markup)
    };
  }
}
