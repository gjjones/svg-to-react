#!/usr/bin/env node
'use strict';

const program = require('commander');

program
  .version('0.0.1')
  .option('-i, --input [glob]', 'Specify input path to SVGs')
  .option('-o, --output [path]', 'Specify output path for react components')
  .parse(process.argv);

const fs = require('fs');
const svgToReact = require('./src/svg-to-react.js');

const componentsData = svgToReact(program.input);

fs.writeFile(program.output, componentsData, 'utf8');
